FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/redirector
COPY . /opt/redirector

RUN npm install

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/redirector/bin/dumb-init", "--"]
CMD ["npm", "start"]
